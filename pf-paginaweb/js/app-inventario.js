import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getDatabase, ref as refS, onValue, set, get, child, remove } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";
import { getStorage, ref, uploadBytesResumable, getDownloadURL, } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyAXQT6W5C_lrOKRZPO-7jSzey8rRFCkBZo",
    authDomain: "pf-paginaweb.firebaseapp.com",
    projectId: "pf-paginaweb",
    storageBucket: "pf-paginaweb.appspot.com",
    messagingSenderId: "276499866095",
    appId: "1:276499866095:web:dac7541f564e25066cd61a"
};

const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage(app);

// DECLARAMOS LOS BOTONES
const btnAgregar = document.getElementById('btnAgregar');
const btnBuscar = document.getElementById('btnBuscar');
const btnActualizar = document.getElementById('btnActualizar');
const btnBorrar = document.getElementById('btnBorrar');
const btnLimpiar = document.getElementById('btnLimpiar');

// SUBIR ARCHIVO
const archivoImg = document.getElementById("archivo");
// CONTENEDOR PARA SUBIR LOS AUTOS
const container = document.getElementById("autos");

// DECLARAMOS
let codigo = "";
let nombreAuto = "";
let costoAuto = "";
let desc = "";
let archivo = "";

// MANDAMOS A LLAMAR A LAS FUNCIONES PARA TENER TODO CARGADO
mostrarAutos();
listarproductos();

// FUNCIONES -------------------------------------------------------------------------------

// LIMPIAR INPUTS
function limpiarInputs() {
    document.getElementById("codigo").value = "";
    document.getElementById("nombreAuto").value = "";
    document.getElementById("costoAuto").value = "";
    document.getElementById("desc").value = "";
    document.getElementById("archivo").value = "";
}

// INSERTAR PRODUCTOS -------------------------------------------------------------------------------
function aggAuto(event) {

    event.preventDefault();

    let codigo = document.getElementById("codigo").value;
    let nombreAuto = document.getElementById("nombreAuto").value;
    let costo = document.getElementById("costoAuto").value;
    let desc = document.getElementById("desc").value;


    const file = archivoImg.files[0];
    let urlImg = "";

    if (file) {
        const storageRef = ref(storage, file.name);
        const uploadTask = uploadBytesResumable(storageRef, file);

        uploadTask.on(
            "state_changed",
            (snapshot) => {
                const progress =
                    (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                alert("Progreso: " + progress.toFixed(2) + "%");
            },
            (error) => {
                alert(error);
            },
            () => {
                getDownloadURL(uploadTask.snapshot.ref)
                    .then((downloadURL) => {
                        urlImg = downloadURL;

                        set(refS(db, "autos/" + codigo), {
                                nombre: nombreAuto,
                                costo: costo,
                                descripcion: desc,
                                urlImg: urlImg
                            })
                            .then(() => {
                                alert("Se insertó con éxito.");
                                limpiarInputs();
                                listarproductos();
                            })
                            .catch((error) => {
                                alert("Ocurrió un error: " + error);
                            });
                    })
                    .catch((error) => {
                        alert(error);
                    });
            }
        );
    }
}


//  MOSTRAR PRODUCTOS -------------------------------------------------------------------------------
function mostrarAutos() {
    const dbRef = refS(db, "autos");

    onValue(
        dbRef,
        (snapshot) => {
            snapshot.forEach((childSnapshot) => {
                const data = childSnapshot.val();
                let id = childSnapshot.key;

                let cartaAuto = `
                <article class="articulo">
                    <img class="img-art" src="${data.urlImg}" alt="Modelo 1">
                    <h2 class="titulo-articulo" ><b>${data.nombre}</b></h2>
                    <h3 class="precio-articulo" >Precio: $${data.costo} mx</h2>
                    <p class="desc-articulo">${data.descripcion}</p>
                </article>
            `;

                container.innerHTML += cartaAuto;
            });
        }, { onlyOnce: true }
    );
}

// FUNCION PARA MOSTRAR LOS AUTOS EN LA TABLA
function listarproductos() {

    const dbRef = refS(db, 'autos');
    const tabla = document.getElementById('tablaAutos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML = '';

    onValue(dbRef, (snapshot) => {
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const data = childSnapshot.val();

            var fila = document.createElement('tr');

            var celdaCodigo = document.createElement('td');
            celdaCodigo.textContent = childKey;
            fila.appendChild(celdaCodigo);

            var celdaNombre = document.createElement('td');
            celdaNombre.textContent = data.nombre;
            fila.appendChild(celdaNombre);

            var celdaPrecio = document.createElement('td');
            celdaPrecio.textContent = "$" + data.costo;
            fila.appendChild(celdaPrecio);

            var celdaDescripcion = document.createElement('td');
            celdaDescripcion.textContent = data.descripcion;
            fila.appendChild(celdaDescripcion);

            var celdaImagen = document.createElement('td');
            var imagen = document.createElement('img');

            imagen.src = data.urlImg;
            imagen.width = 300;
            celdaImagen.appendChild(imagen);
            fila.appendChild(celdaImagen);

            tbody.appendChild(fila);
        });
    }, { onlyOnce: true });
}

// FUNCION ELIMINAR
function eliminarAuto(event) {
    let id = document.getElementById('codigo').value;
    if (id === "") {
        return alert("No se ingresó un Codigo válido.");;
    }
    const dbref = refS(db);
    get(child(dbref, 'autos/' + id)).then((snapshot) => {
        if (snapshot.exists()) {
            remove(refS(db, 'autos/' + id))
                .then(() => {
                    alert("Producto eliminado con éxito.");
                    limpiarInputs();
                    mostrarAutos();
                    listarproductos();
                })
                .catch((error) => {
                    alert("Ocurrió un error al eliminar el producto: " + error);
                });
        } else {
            limpiarInputs();
            alert("El producto con ID " + id + " no existe.");
        }
    });

}
// FUNCION BUSCAR
function buscarAuto() {
    let id = document.getElementById('codigo').value.trim();
    if (id === "") {
        mostrarMensaje("No se ingresó código.");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'autos/' + id)).then((snapshot) => {
        if (snapshot.exists()) {
            nombreAuto = snapshot.val().nombre;
            costoAuto = snapshot.val().costo;
            desc = snapshot.val().descripcion;
            archivo = snapshot.val().urlImg;

            // ESCRIBIMOS LOS INPUTS
            document.getElementById('nombreAuto').value = nombreAuto;
            document.getElementById('costoAuto').value = costoAuto;
            document.getElementById('desc').value = desc;
            document.getElementById('archivo').value = urlImg;

        } else {
            limpiarInputs();
            mostrarMensaje("El producto con código " + id + " no existe.");
        }
    });
}

// FUNCION ACTUALIZAR
function actualizarAuto(event) {

    let codigo = document.getElementById("codigo").value;
    let nombreAuto = document.getElementById("nombreAuto").value;
    let costo = document.getElementById("costoAuto").value;
    let desc = document.getElementById("desc").value;

    const container = document.getElementById("autos");
    const file = archivoImg.files[0];
    let urlImg = "";

    if (file) {
        const storageRef = ref(storage, file.name);
        const uploadTask = uploadBytesResumable(storageRef, file);

        uploadTask.on(
            "state_changed",
            (snapshot) => {
                const progress =
                    (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                alert("Progreso: " + progress.toFixed(2) + "%");
            },
            (error) => {
                alert(error);
            },
            () => {
                getDownloadURL(uploadTask.snapshot.ref)
                    .then((downloadURL) => {
                        urlImg = downloadURL;

                        set(refS(db, "autos/" + codigo), {
                                nombre: nombreAuto,
                                costo: costo,
                                descripcion: desc,
                                urlImg: urlImg
                            })
                            .then(() => {
                                alert("Se insertó con éxito.");
                                limpiarInputs();
                                listarproductos();
                            })
                            .catch((error) => {
                                alert("Ocurrió un error: " + error);
                            });
                    })
                    .catch((error) => {
                        alert(error);
                    });
            }
        );
    }
}

// BOTONES Y SUS FUNCIONES --------------------------------------------------------------------------------
// BOTON AGREGAR
btnAgregar.addEventListener("click", (event) => {
    aggAuto(event);
    listarproductos();
});

// BOTON LIMPIAR
btnLimpiar.addEventListener("click", () => {
    limpiarInputs();
});
// BOTON ELIMINAR 
btnBorrar.addEventListener("click", () => {
    eliminarAuto();
});
// BOTON BUSCAR 
btnBuscar.addEventListener("click", () => {
    buscarAuto();
});
// BOTON ACTUALIZAR 
btnActualizar.addEventListener("click", (event) => {
    actualizarAuto(event);
});