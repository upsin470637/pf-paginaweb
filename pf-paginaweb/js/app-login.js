// Import the functions you need from the SDKs you need
import { initializeApp } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js';
import { getAuth, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js'
import { getDatabase, onValue, ref as refS, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";
import { getStorage, ref, uploadBytesResumable, getDownloadURL } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";


const firebaseConfig = {
    apiKey: "AIzaSyAXQT6W5C_lrOKRZPO-7jSzey8rRFCkBZo",
    authDomain: "pf-paginaweb.firebaseapp.com",
    projectId: "pf-paginaweb",
    storageBucket: "pf-paginaweb.appspot.com",
    messagingSenderId: "276499866095",
    appId: "1:276499866095:web:dac7541f564e25066cd61a"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage(app);
const auth = getAuth(app);

const btnLogin = document.getElementById('btnLogin');

function login() {
    const email = document.getElementById('correo').value;
    const passwd = document.getElementById('password').value;

    signInWithEmailAndPassword(auth, email, passwd).then(() => {
            alert("Se inicio sesion correctamente");
            window.location.href = "/html/inventario.html";
        })
        .catch(() => {
            alert("El correo o la contraseña son incorrectas");
        });
}

btnLogin.addEventListener('click', () => {
    login();
});